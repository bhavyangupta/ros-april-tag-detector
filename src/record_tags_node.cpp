/**
 * Copyright (c) 2015, Bhavya Narain Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */


/*
 * record_tags_node.cpp
 * 
 */
#include "video.hpp"
#include "image_msg.hpp"
#include "april_tag_detector.hpp"
#include <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <ctime>

using std::string;
using std::cout;
using std::endl;

int main(int argc, char **argv){
  #ifdef ROS_RELEASE
  ros::init(argc,argv,"tag_recoder");
  ros::NodeHandle nh("~");
  #endif

  // ********* All this can be made rosparams
  bool   display_input_video = true;
  bool   display_tag_info    = true;
  bool   tag_detected        = false;
  bool   show_tag_on_video   = true;
  double tag_size_mtr        = 0.166;
  double focal_length_x_px   = 1040.63;
  double focal_length_y_px   = 1041.25;
  double image_width_px      = 1920;
  double image_height_px     = 1080;
  int    frame_counter       = 1;
  string video_filename      = argv[1];
  //**************************************
  time_t             Tstart,Tend;
  video              video_file(video_filename,display_input_video,30);
  #ifdef ROS_RELEASE
  image_msg          frame_msg(nh);
  #endif
  cv::Mat            next_frame;
  april_tag_detector tag_detector(image_width_px, image_height_px, tag_size_mtr,
                                  focal_length_x_px, focal_length_y_px,
                                  display_tag_info);
  
  tag_detector.print_string_linespec();
  Tstart = time(0);
  while(video_file.peek_next_frame()){
    // cout<<"Processing frame: "<<frame_counter<<endl;
    next_frame = video_file.get_next_frame();
    tag_detected = tag_detector.detect_tags(next_frame,show_tag_on_video);
    if(tag_detected){
      tag_detector.get_tag_info();
    }
    #ifdef ROS_RELEASE 
    frame_msg.publish_frame(next_frame);
    #endif
    video_file.show_frame();
    frame_counter++;
  }
  Tend = time(0);
  cout<<"Time taken: "<<difftime(Tend,Tstart)<<" s"<< endl;
  return 0;
}
