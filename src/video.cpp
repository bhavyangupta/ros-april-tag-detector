/**
 * Copyright (c) 2015, Bhavya Narain Gupta
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 */

#include "video.hpp"

video::video(string filename, bool display_frame,int fps)
:filename(filename),
 fps(fps),
 display_frame(display_frame)
{
  if(!open_file()){
    std::cout<<"Video file cannot be opened"<<std::endl;  
    exit(-1); 
  }
  std::cout<<"Video opened: "<<filename<<std::endl; 
  std::cout<<"fps "<<fps<<std::endl;
  if(display_frame){
    cv::namedWindow("Input video",CV_WINDOW_AUTOSIZE);
  }
}

void video::show_frame(){
  if(display_frame){
    cv::imshow("Input video",this->next_frame);
    cv::waitKey(1000/fps);
  }
}

bool video::peek_next_frame(){
  bool success = capture_source.read(next_frame);
  return success;
}

cv::Mat video::get_next_frame(){
  next_frame.convertTo(next_frame,CV_8UC3);
  return next_frame;
}

bool video::open_file(){
  capture_source = cv::VideoCapture();
  capture_source.open(filename);
  bool status = capture_source.isOpened();
  return status;
}
