# april_tag_detector

- **How is this package different?** It runs tag detection
on each frame and is not affected by the speed of detection.
If you use a ROS node to do this, finite input queue depth
will inevitably lead in frame loss due to slow detection.

- **When to use this over ROS package?**
 + When realtime detection is not required.
 + Processing of each frame is required.
 + Detection is being done on high-res images.

## Notes on usage:
- Input video is provided as a runtime argument. Tested 
formats are .mp4 and .mkv.

- Results are printed on the terminal and can be 
redirected to text files. 

- ROS support can be enabled at build time by 
setting a build flag in the CMakeLists.txt file.
Note that the code just publishes the video to a rostopic
and the detections would still need to be stored
via redirection.

- Output format is printed on the terminal when the code
starts executing and should be saved on the output file
automatically. Here it is for reference:

```
frame_number,number of tags,id_1,h111,h112,h113,h114,h121,h122,h123,h124,h131,h132,h133,h134,h141,h142,h143,h144,.....
```

- Important parameters and the main function is in the 
record_tag_nodes.cpp file.